<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-04-23
 * Time: 10:01
 */

/**
 * Generate a random hexadecimal token by hashing the current time in microseconds as float
 *
 * @return string Random 32-characters long hexadecimal token
 */
function generateToken() {
    return md5(microtime(true));
}