<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;


Auth::routes();

/**
 * User
 * */
Route::name('users::')->prefix('users')->group(function () {
    Route::get('{user}/{token}/confirm', 'Auth\\RegisterController@confirm')->name('confirm');
    Route::get('{user}/change/pagination/{variant}', 'UserController@changePagination')->name('changePagination');
});

/**
 * Vendor
 * */
Route::name('vendor.')->prefix('vendor')->group(function () {
    Route::get('/', 'VendorController@index')->name('paginate');
    Route::post('remote/shop/', 'VendorController@remoteShop')->name('remote.shop');
});

/**
 * Admin
 * */
Route::name('admin.')->prefix('admin')->group(function () {
    Route::get('/', 'AdminController@index')->name('dashboard');
    Route::post('/register', 'AdminController@registerUser')->name('registerUser');
    Route::patch('/users/{user_id}', 'AdminController@updateUser')->name('updateUser');
    Route::delete('/users/{user_id}', 'AdminController@deleteUser')->name('deleteUser');
});

/**
 * Product
 * */
Route::name('product.')->group(function () {
    Route::get('/', 'ProductController@product')->name('paginate');

    Route::name('current.')->group(function () {
        Route::get('/price/{product_id}', 'ProductController@selling_price')->name('selling_price');
    });
});

/**
 * This show alert after notification with timeout
 * */
Route::group(['prefix' => 'preview', 'as' => 'preview.'], function (){
    Route::group(['prefix' => 'alert', 'as' => 'alert.'], function () {
        Route::get('/{message}/{time}/{redirect_to?}', 'PreviewController@index')->name('message')
            ->where(['time' => '[0-9]+', 'redirect_to' => '[a-z]+']);;
    });
});

//trigger the scheduler
Route::get('/hshhdyw782qgN3Paammxh29' , function(){
    Artisan::call('schedule:run');
    return 'OK';
});