<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RemoteShop extends Model
{
    protected $table = 'remote_shops';

    protected $connection = "mysql_amoritalia";

    protected $fillable = ['vendor_id', 'options', 'type'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'array',
    ];

    /**
     * Get owner of shop
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function vendor()
    {
        return $this->hasOne(User::class, 'user_id', 'vendor_id');
    }
}
