<?php

namespace App\Amoritalia;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    protected $connection = 'mysql_amoritalia';

    protected $table = 'product_models';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'product_id');
    }
}
