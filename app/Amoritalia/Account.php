<?php

namespace App\Amoritalia;

use App\User;
use App\RemoteShop;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property mixed type
 * @property mixed user_id
 */
class Account extends Model
{
    protected $connection = 'mysql_amoritalia';

    protected $table = 'user';

    protected $with = ['remoteShop'];

    public function isVendor(){
        return ($this->type == 2);
    }

    public function isSupplier(){
        return ($this->type == 1);
    }

    public function isAdmin(){
        return ($this->type == 0);
    }

    /**
     * Get all remote shop
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function remoteShop(){
       return $this->hasMany(RemoteShop::class, 'vendor_id', 'user_id');
    }
}
