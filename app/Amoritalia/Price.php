<?php

namespace App\Amoritalia;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $connection = 'mysql_amoritalia';

    protected $table = 'price_selling';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product(){
        return $this->belongsTo(Product::class, 'product_id', 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vendor(){
        return $this->belongsTo(Account::class, 'vendor_id', 'user_id');
    }
}
