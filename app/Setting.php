<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed id
 * @property mixed user_id
 * @property mixed options
 * @property mixed accounts_supplier
 * @property mixed accounts_vendor
 */
class Setting extends Model
{
    const PAGINATION = ['cards', 'table']; // minutes

    protected $table = 'setting_user';

    protected $fillable = ['user_id', 'options', 'accounts_supplier', 'accounts_vendor'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'array',
        'accounts_supplier' => 'array',
        'accounts_vendor' => 'array',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
