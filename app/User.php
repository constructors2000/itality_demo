<?php

namespace App;

use App\Amoritalia\Account;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property mixed id
 * @property Collection roles
 * @property mixed email_confirmation_token
 * @property static email_confirmation_at
 * @property Setting setting
 * @property mixed user_verified_at
 */
class User extends Authenticatable
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'email_confirmation_token', 'user_verified_at',
    ];

    protected $with = ['setting'];

    /**
     * The attributes that should be casted to a Carbon instance.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'confirmed_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_confirmation_at' => 'datetime',
        'confirmed_at' => 'datetime',
        'user_verified_at' => 'datetime',
    ];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function setting()
    {
        return $this->hasOne(Setting::class, 'user_id', 'id');
    }

    /**
     * @param string|array $roles
     * @return bool
     */
    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) ||
                abort(401, 'This action is unauthorized.');
        }
        return $this->hasRole($roles) ||
            abort(401, 'This action is unauthorized.');
    }

    /**
     * Check multiple roles
     * @param array $roles
     * @return bool
     */
    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    /**
     * Check one role
     * @param string $role
     * @return bool
     */
    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }

    /**
     * Check if the email_confirmation_token field in the database is NULLed or not
     *
     * @return bool Whether the user has confirmed his e-mail address or not
     */
    public function hasConfirmed()
    {
        return $this->email_confirmation_token === null ? true : false;
    }

    /**
     * Try to confirm the user's e-mail address using the provided token
     *
     * @param $token string The user-provided token
     * @return bool Whether the confirmation succeed or not.
     */
    public function confirm($token)
    {
        // If the user has already confirmed we can't confirm him again.
        if ($this->hasConfirmed()) return false;
        if ($token === $this->email_confirmation_token) {
            // User has confirmed his e-mail address.
            $this->email_confirmation_token = null;
            $this->email_confirmation_at = \Carbon\Carbon::now();
            $this->save();
            return true;
        }
        // Token was incorrect.
        return false;
    }

    /**
     * Try to unconfirm the user's e-mail address
     *
     * @return bool Whether the unconfirmation succeed or not.
     */
    public function unconfirm()
    {
        // If the user is not even confirmed we can't unconfirm him.
        if (!$this->hasConfirmed()) return false;
        // Reset token with a newly generated one and save the model.
        $this->email_confirmation_token = generateToken();
        $this->email_confirmation_at = null;
        $this->save();
        return true;
    }
}
