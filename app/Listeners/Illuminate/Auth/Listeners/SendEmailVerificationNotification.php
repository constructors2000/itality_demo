<?php

namespace App\Listeners\Illuminate\Auth\Listeners;

use App\Notifications\ConfirmUserMail;
use App\Notifications\UserCreate;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class SendEmailVerificationNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param Registered $event
     * @return void
     */
    public function handle(Registered $event)
    {
        if ($event->user instanceof User) {
            Notification::send($event->user, new ConfirmUserMail());
        }
    }
}
