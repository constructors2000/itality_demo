<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-04-05
 * Time: 09:06
 */

namespace App\Query;

use App\Amoritalia\Image;
use App\Amoritalia\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Factory;

class ProductQuery
{
    private $query;

    private $select;

    /**
     * Map List for filter only for products tables
     * */
    public const ColumnMapFilter = [
        'filter_groups' => 'products.product_group_id',
        'filter_categories' => 'products.category_id',
        'filter_sub_categories' => 'products.sub_cat_id',
        'filter_brands' => 'products.brand',
        'filter_product_name' => 'products.product_name',
        'filter_product_id' => 'products.product_id',
        'filter_product_code' => 'products.product_code',
        'filter_product_supplier_id' => 'products.unique_id',
        'filter_active' => 'products.active',
    ];

    /**
     * Map List for filter another tables
     * */
    public const ExtColumnMapFilter = [
        'filter_vendors' => ['table' => 'vendor_data_permissions', 'column' => 'vendor_data_permissions.vendor_id', 'key' => 'filter_vendors'],
        'filter_image' => ['table' => 'product_images', 'column' => 'product_images.image_url', 'key' => 'filter_image'],
        'filter_made' => ['table' => 'feture_detail', 'column' => 'feture_detail.feature_value', 'key' => 'filter_made'],
    ];

    /**
     * Init
     * */
    public function __construct()
    {
        $this->query = Product::query();

        $this->select = collect([
            'products.product_id as product.id',
            'products.product_code as product.code',
            'products.product_name as product.name',
            'products.availability as product.availability',
            'products.brand as product.brand',
            'products.active as product.active',
            'products.unique_id as product.unique_id',
        ]);
    }


    /**
     * Prepare query
     *
     * @return Builder
     */
    public function get()
    {
        /**
         * Add image
         * */
        $this->select = $this->select->merge([DB::raw("({$this->imageSubQuery()->toSql()}) as `product.images`")]);

        /**
         * Add group
         * */
        $this->select = $this->select->merge([
            'product_group.product_group_id as group.id',
            'product_group.group_name as group.name',
        ]);
        $this->query->leftJoin('product_group', 'product_group.product_group_id', '=', 'products.product_group_id');

        /**
         * Add category
         * */
        $this->select = $this->select->merge([
            'categorys.category_id as category.id',
            'categorys.category_name as category.name',
        ]);
        $this->query->leftJoin('categorys', 'categorys.category_id', '=', 'products.category_id');

        /**
         * Add Sub-category
         * */
        $this->select = $this->select->merge([
            'sub_category.sub_cat_id as sub_category.id',
            'sub_category.sub_cat_name as sub_category.name',
        ]);
        $this->query->leftJoin('sub_category', 'sub_category.sub_cat_id', '=', 'products.sub_cat_id');

        /**
         * Add Supplier
         * */
        $this->select = $this->select->merge([
            'user.user_id as supplier.id',
            'user.display_name as supplier.name',
        ]);
        $this->query->leftJoin('user', 'user.user_id', '=', 'products.supplier_id');

        $this->query->select($this->select->toArray());
        return $this->query;
    }


    /**
     * SubQuery for select image width product
     *
     * @return Builder
     */
    private function imageSubQuery()
    {
        $query = Image::query();
        return $query->select([
            DB::raw("CONCAT('[',GROUP_CONCAT(CONCAT('{\"url\":\"',product_images.image_url, '\"','}')),']') AS img")
        ])->whereRaw('`product_images`.`product_id` = `products`.`product_id`');
    }

    /**
     * Create SubQuery for filter
     *
     * @param Request $request
     * @return ProductQuery
     */
    public function filter(Request $request)
    {
        /**
         * Get array for filtering
         * @var array $filter
         * */
        $filter = $request->only(array_keys((ProductQuery::ColumnMapFilter)));

        foreach ($filter as $key => $inputs) {
            if ((empty($inputs) && !is_numeric($inputs)) || (is_numeric($inputs) && (int)$inputs < 0))
                continue;

            $this->query->where(function ($query) use ($key, $inputs) {
                $where = 'where';
                /* if filter by array */
                if (is_array($inputs)) {
                    foreach ($inputs as $input) {
                        $operator = "=";
                        if (!is_numeric($input)) {
                            $operator = 'Like';
                            $input = "%{$input}%";
                        }
                        $query->$where(ProductQuery::ColumnMapFilter[$key], $operator, $input);
                        $where = 'orWhere';
                    }
                } else {
                    $operator = "=";
                    if (!is_numeric($inputs)) {
                        $operator = 'Like';
                        $inputs = "%{$inputs}%";
                    }
                    $query->$where(ProductQuery::ColumnMapFilter[$key], $operator, $inputs);
                }
                return $query;
            });
        }

        /**
         * Extension filtering
         * */
        $filters = $request->only(array_keys((ProductQuery::ExtColumnMapFilter))) ?: [];

        if (!empty($filters['filter_vendors']))
            $this->filterByVendors($filters['filter_vendors']);
        if (!empty($filters['filter_image']) && $filters['filter_image'] > 0)
            $this->filterByImageExist($filters['filter_image']);
        if (!empty($filters['filter_made']))
            $this->filteringByMade($filters['filter_made']);

        return $this;
    }

    /**
     * Add vendor permission query to main query
     *
     * @param array $filters
     */
    private function filterByVendors(array $filters)
    {
        $this->query->where(function ($query) use ($filters) {

            $where_vendor = 'where';
            foreach ($filters as $vendor_id) {
                $permissions = $this->getVendorPermissions($vendor_id);
                $query->$where_vendor(function ($query) use ($permissions) {
                    $where_group = 'orWhere';
                    foreach ($permissions as $permission_row) {
                        $query->$where_group(function ($query) use ($permission_row) {
                            foreach ($permission_row as $column => $item) {
                                $query->where($column, '=', $item);
                            }
                        });
                        $where_group = 'orWhere';
                    }
                });

                $where_vendor = 'orWhere';
            }

            return $query;
        });
    }

    /**
     * Filtering if product exist images or not
     *
     * @param bool $status
     */
    private function filterByImageExist($status)
    {
        $where = $status ? 'whereNull' : 'whereNotNull';
        $this->query->leftJoin('product_images as pi', function ($query) {
            $query->whereRaw('`pi`.`product_id` = `products`.`product_id`');
        })->$where('pi.image_id');
    }

    /**
     * Filtering made product
     *
     * @param $countries
     */
    private function filteringByMade($countries)
    {
        $this->query->whereExists(function ($query) use ($countries) {

            $query->from('feture_detail as fd')->whereRaw('fd.product_id = products.product_id');

            $query->where(function ($query) use ($countries) {
                $where = 'whereRaw';
                foreach ($countries as $country) {
                    $query->$where('LOWER(fd.feature_value)  LIKE  "%made%"');
                    $query->$where('LOWER(fd.feature_value)  LIKE  "%' . $country . '%"');
                    $where = "orWhereRaw";
                }
            });

        });
    }

    /**
     * Get all vendor permission.
     *
     * @param integer $id
     * @return array
     */
    private function getVendorPermissions($id)
    {
        $vendorPermissions = DB::connection('mysql_amoritalia')
            ->table('vendor_data_permissions')
            ->where('vendor_id', '=', $id)
            ->get();

        $vendorPermissions = $vendorPermissions->toArray();
        $_permissions = [];
        foreach ($vendorPermissions as $i => $vendorPermission) {

            foreach ($vendorPermission as $permission => $id) {
                if (empty($id) ||
                    stripos($permission, 'vender_permission_id') !== false ||
                    stripos($permission, 'gross_margin_range') !== false ||
                    stripos($permission, 'vendor') !== false
                ) continue;

                if (stripos($permission, 'sub_category_id') !== false)
                    $permission = 'sub_cat_id';

                $_permissions[$i]["products.$permission"] = $id;
            }
        }

        return $_permissions;
    }


    /**
     * Validate Filter fields
     *
     * @param Request $request
     * @return void
     */
    public static function requestValidate(Request $request)
    {
        /** @var Validator $validator */
        $validator = Validator::make($request->all(), [
            'filter_groups' => ['array'],
            'filter_groups.*' => ['integer', 'exists:mysql_amoritalia.product_group,product_group_id'],
            'filter_categories' => ['array'],
            'filter_categories.*' => ['integer', 'exists:mysql_amoritalia.categorys,category_id'],
            'filter_sub_categories' => ['array'],
            'filter_sub_categories.*' => ['integer', 'exists:mysql_amoritalia.sub_category,sub_cat_id'],

            'filter_suppliers' => ['array'],
            'filter_suppliers.*' => ['integer', 'exists:mysql_amoritalia.user,user_id,type,1'],
            'filter_vendors' => ['array'],
            'filter_vendors.*' => ['integer', 'exists:mysql_amoritalia.user,user_id,type,2'],
            'filter_brands' => ['array'],
            'filter_brands.*' => ['string', 'exists:mysql_amoritalia.brands,name'],

            'filter_active.*' => ['integer', 'in:9,0,1'],
        ]);

        $validator->validate();
    }
}
