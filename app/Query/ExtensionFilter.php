<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-04-24
 * Time: 13:40
 */

namespace App\Query;


use Illuminate\Support\Facades\DB;

class ExtensionFilter
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getGroups()
    {
        return DB::connection('mysql_amoritalia')
            ->table('product_group')
            ->select(['product_group_id as id', 'group_name as name'])->get();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getCategories()
    {
        return DB::connection('mysql_amoritalia')
            ->table('categorys')
            ->select(['category_id as id', 'category_name as name'])
            ->get();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getSubCategories()
    {
        return DB::connection('mysql_amoritalia')
            ->table('sub_category')
            ->select(['sub_cat_id as id', 'sub_cat_name as name'])
            ->get();
    }

    public static function getBrands(){
        return DB::connection('mysql_amoritalia')
            ->table('brands')
            ->select(['id', 'name'])
            ->get();
    }
}
