<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-04-24
 * Time: 13:43
 */

namespace App\Query;


use App\Role;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PriceQuery
{
    public static function selling_price($product_id)
    {

        $price_selling = DB::connection('mysql_amoritalia')
            ->table('price_selling')
            ->select([
                'price_selling.itality_selling_price as price.itality_selling',
                'price_selling.vendor_rrp as price.vendor_rrp',
                'price_selling.net_margin as price.net_margin',
                'user.user_id as supplier.id',
                'user.display_name as supplier.name',
            ])
            ->where('price_selling.product_id', '=', $product_id)
            ->leftJoin('user', 'user.user_id', '=', 'price_selling.vendor_id');


        /** @var User $user */
        $user = Auth::user();

        if (!$user->hasAnyRole([Role::available_roles[0], Role::available_roles[1]])) {
            $accounts_vendor = [0];
            if ($user->hasRole('vendor') && !empty($user->setting->accounts_vendor)) {
                $accounts_vendor = $user->setting->accounts_vendor;
            }
            $price_selling->whereIn('price_selling.vendor_id', $accounts_vendor);
        }
        else if ($user->hasRole('supplier') && !empty($user->setting->accounts_vendor)) {
            $price_selling->whereIn('price_selling.vendor_id', $user->setting->accounts_vendor);
        }

        return view('product.current.price', ['price' => $price_selling->get()]);
    }
}