<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-04-24
 * Time: 10:15
 */

namespace App\Query;


use App\Amoritalia\Account;
use Illuminate\Database\Query\Builder;

class AccountQuery
{
    /**
     * Get active accounts
     *
     * @param int $type 0 - admin; 1 - supplier; 2 - vendor;
     * @param array $ids
     * @return \Illuminate\Support\Collection
     */
    private static function getAccounts(int $type = null, $ids = [])
    {
        /** @var Builder $accounts */
        $accounts = Account::query();

        $accounts->select(['user_id as id', 'display_name as name']);

        $accounts->where([['active', '=', 0]]);

        if (!is_null($type))
            $accounts->where('type', '=', $type);

        if(!empty($ids))
            $accounts->whereIn('user_id', $ids);

        return $accounts->get();
    }

    /**
     * Vendors
     *
     * @param array $ids
     * @return \Illuminate\Support\Collection
     */
    public static function getVendorAccounts($ids = []){
        return self::getAccounts(2, $ids);
    }

    /**
     * Supplier
     *
     * @param array $ids
     * @return \Illuminate\Support\Collection
     */
    public static function getSupplierAccounts($ids = []){
        return self::getAccounts(1, $ids);
    }

    /**
     * Admins
     *
     * @param array $ids
     * @return \Illuminate\Support\Collection
     */
    public static function getAdminAccounts($ids = []){
        return self::getAccounts(0, $ids);
    }


    /**
     * All
     *
     * @param array $ids
     * @return \Illuminate\Support\Collection
     */
    public static function getAllAccounts($ids = []){
        return self::getAccounts(null, $ids);
    }
}
