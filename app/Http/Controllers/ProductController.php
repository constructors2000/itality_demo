<?php

namespace App\Http\Controllers;

use App\Amoritalia\Group;
use App\Amoritalia\Product;
use App\Query\AccountQuery;
use App\Query\ExtensionFilter;
use App\Query\PriceQuery;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Query\ProductQuery;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function product(Request $request)
    {
        /**
         * Validate filter
         * */
        $this->validateQuery($request);

        $per_page = $request->has('per_page') ? $request->get('per_page') : 25;

        if (!$request->has('filter_active')) {
            $request->request->add(['filter_active' => 0]);
        }

        if (!$request->has('filter_image')) {
            $request->request->add(['filter_image' => 0]);
        }

        /** @var User $user */
        $user = Auth::user();

        $filter_suppliers = $filter_vendors = [];

        /**
         * If user is not Admin
         * */
        if (!$user->hasRole(Role::available_roles[0])) {

            /**
             * If user not has accounts we set filter supplier 0 and filter_vendors 0
             * */
            if (empty($user->setting->accounts_supplier) && empty($user->setting->accounts_vendor)) {
                $request->request->add(['filter_suppliers' => [0], 'filter_vendors' => [0]]);
                $filter_suppliers = $filter_vendors = [0];
            } else {
                /**
                 * IF user has accounts_supplier array (seated by Admin on Admin page)
                 * */
                if (!empty($user->setting->accounts_supplier)) {
                    if (!($request->has('filter_suppliers') && empty(array_diff($request->input('filter_suppliers'), $user->setting->accounts_supplier)))) {
                        $request->request->add(['filter_suppliers' => $user->setting->accounts_supplier]);
                    }
                } else {
                    $request->request->add(['filter_suppliers' => []]);
                }
                /**
                 * IF user has accounts_vendor array (seated by Admin on Admin page)
                 * */
                if (!empty($user->setting->accounts_vendor)) {
                    if (!($request->has('filter_vendors') && empty(array_diff($request->input('filter_vendors'), $user->setting->accounts_vendor)))) {
                        $request->request->add(['filter_vendors' => $user->setting->accounts_vendor]);
                    }
                } else {
                    $request->request->add(['filter_vendors' => []]);
                }

                $filter_suppliers = !empty($user->setting->accounts_supplier) ? $user->setting->accounts_supplier : [0];
                $filter_vendors = !empty($user->setting->accounts_vendor) ? $user->setting->accounts_vendor : [0];
            }
        }

        /**
         * Get product
         * */
        $products = (new ProductQuery())->filter($request);

        $products = $products->get();

        $inputs = $request->all();

        foreach ($inputs as $key => $input)
            if (is_array($input))
                $inputs[$key] = array_unique($input);

        $products = $products->paginate($per_page)->appends($inputs);

        if ($request->ajax()) {
            return $this->getAjaxProductList($products);
        }

        $filter = [
            'groups' => ExtensionFilter::getGroups(),
            'brands' => ExtensionFilter::getBrands(),
            'vendors' => AccountQuery::getVendorAccounts($filter_vendors),
            'suppliers' => AccountQuery::getSupplierAccounts($filter_suppliers),
            'categories' => ExtensionFilter::getCategories(),
            'sub_categories' => ExtensionFilter::getSubCategories(),

        ];


        return view('product.index', ['products' => $products, 'filter' => $filter, 'url' => $request->fullUrlWithQuery($request->toArray())]);
    }

    /**
     * Get table of products
     *
     * @param $products
     * @return array|string
     * @throws \Throwable
     */
    private function getAjaxProductList($products)
    {
        /** @var User $user */
        $user = Auth::user();

        $options = isset($user->setting->options) ? $user->setting->options : [];
        $container = \App\Setting::PAGINATION['0'];
        if (!empty($options['product_pagination']))
            $container = $options['product_pagination'];

        return view("product.list.{$container}.container", ['products' => $products])->render();
    }

    /**
     * Validator
     * @param Request $request
     */
    protected function validateQuery(Request $request)
    {
        $this->validate($request, [
            'per_page' => ['integer', 'max:500'],
            'page' => ['integer']
        ]);

        ProductQuery::requestValidate($request);
    }


    public function selling_price($product_id)
    {
        if (!is_numeric($product_id) && !(Product::find($product_id)))
            abort(404);

        return PriceQuery::selling_price($product_id);
    }
}
