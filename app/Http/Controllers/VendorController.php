<?php

namespace App\Http\Controllers;

use App\Http\Resources\VendorCollection;
use App\Query\AccountQuery;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VendorController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|vendor']);
    }

    /**
     * Show vendor setting page
     * Limit list vendor by role or vendor account
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        /** @var User $user */
        $user = Auth::user();

        $filter_vendors = [0];

        if ($user->hasRole(Role::available_roles[0])) {
            $filter_vendors = [];
        } elseif (!empty($user->setting->accounts_vendor)) {
            $filter_vendors = $user->setting->accounts_vendor;
        }

        $staff = AccountQuery::getVendorAccounts($filter_vendors);

        return view('vendor.index', ['vendors' => $staff]);
    }

    /**
     * Create new remote shop
     * @param Request $request
     */
    public function remoteShop(Request $request)
    {
        dd($request->toArray());
    }
}
