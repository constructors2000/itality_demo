<?php

namespace App\Http\Controllers;

use App\Setting;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function changePagination(User $user, $variant, Request $request)
    {
        abort_if(!in_array($variant, Setting::PAGINATION), 400, 'Not allowed variant');

        $options = $user->setting->options;
        $options['product_pagination'] = $variant;

        $user->setting->options = $options;
        $user->setting->save();

        return $variant;
    }
}
