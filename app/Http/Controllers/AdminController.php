<?php

namespace App\Http\Controllers;

use App\Query\AccountQuery;
use App\Role;
use App\Setting;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Amoritalia\Account;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AdminController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','role:admin']);
    }

    public function index(Request $request)
    {
        $per_page = 25;

        $this->validate($request, [
            'per_page' => ['integer', 'max:500'],
            'page' => ['integer']
        ]);

        if ($request->has('per_page'))
            $per_page = $request->get('per_page');

        $accounts_supplier = AccountQuery::getSupplierAccounts();
        $accounts_vendor = AccountQuery::getVendorAccounts();

        $users = User::paginate($per_page)->appends($request->all());

        if ($request->ajax()) {
            return $this->getAjaxUser($users, $accounts_supplier, $accounts_vendor);
        }

        return view('admin.index', [
            'users' => $users,
            'accounts_supplier' => $accounts_supplier,
            'accounts_vendor' => $accounts_vendor
        ]);
    }

    /**
     * Get table of user
     *
     * @param $users
     * @param $accounts_supplier
     * @param $accounts_vendor
     * @return array|string
     * @throws \Throwable
     */
    private function getAjaxUser($users, $accounts_supplier, $accounts_vendor)
    {
        return view('admin.list.user_pagination', [
            'users' => $users,
            'accounts_supplier' => $accounts_supplier,
            'accounts_vendor' => $accounts_vendor
        ])->render();
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function registerUser(Request $request)
    {
        $this->validator($request->all())->validate();
        $this->validateAddition($request->all())->validate();
        event(new Registered($user = $this->createUser($request->all())));

        $accounts_supplier = AccountQuery::getSupplierAccounts();
        $accounts_vendor = AccountQuery::getVendorAccounts();

        $html = view('admin.list.user_row', [
            'user' => $user,
            'accounts_supplier' => $accounts_supplier,
            'accounts_vendor' => $accounts_vendor
        ])->render();

        return response()->json(['html' => $html, 'data' => $user]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     * @throws \Throwable
     */
    public function createUser(array $data)
    {
        $this->checkRole();

        DB::beginTransaction();

        try {

            /** @var User $user */
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password'])
            ]);

            $user->setting()->create([
                'options' => ['product_pagination' => 0],
                'accounts_supplier' => (!empty($data['accounts_supplier']) ? $data['accounts_supplier'] : []),
                'accounts_vendor' => (!empty($data['accounts_vendor']) ? $data['accounts_vendor'] : [])
            ]);

            foreach ($data['roles'] as $role) {
                $user->roles()->attach(Role::where('name', $role)->first());
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response($e->getMessage(), 500);
        }

        return $user;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255', 'min:5'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Get a validator for Addition (account & roles).
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateAddition(array $data)
    {
        return Validator::make($data, [
            'roles' => ['required', 'array', 'in:' . implode(",", Role::available_roles) . ''],
            'accounts_supplier' => ['array'],
            'user_verified' => [Rule::in(['on', 'off'])],
            'accounts_supplier.*' => ['exists:mysql_amoritalia.user,user_id'],
            'accounts_vendor' => ['array'],
            'accounts_vendor.*' => ['exists:mysql_amoritalia.user,user_id']
        ]);
    }

    /**
     * Update accounts for user
     *
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function updateUser(User $user, Request $request)
    {
        $data = $request->all();

        $this->validateAddition($request->all())->validate();

        if(empty($user->setting)) {
            $user->setting()->create([]);
            $user->refresh();
        }

        if(!empty($user->user_verified_at) && $request->input('user_verified') == 'off'){
            $user->user_verified_at = null;
        } elseif(empty($user->user_verified_at) && $request->input('user_verified') == 'on'){
            $user->user_verified_at = \Carbon\Carbon::now();
        }

        $user->save();

        $user->setting->accounts_supplier = !empty($data['accounts_supplier']) ? $data['accounts_supplier'] : [];
        $user->setting->accounts_vendor = !empty($data['accounts_vendor']) ? $data['accounts_vendor'] : [];
        $user->setting->save();

        $user->roles()->detach();

        foreach ($data['roles'] as $role) {
            if ($user->roles->contains('name', $role))
                continue;

            $user->roles()->attach(Role::where('name', $role)->first());
        }

        return response('Update');
    }

    /**
     * Delete user
     *
     * @param User $user
     * @return bool|null
     * @throws \Exception
     */
    public function deleteUser(User $user)
    {
        $user->roles()->detach();
        $user->setting()->delete();
        $user->delete();

        return response('Delete');
    }

    /**
     * Check if role exists
     */
    protected function checkRole()
    {
        if (!(Role::count())) {
            foreach (Role::available_roles as $role) {
                Role::create(['name' => $role]);
            }
        }
    }

}
