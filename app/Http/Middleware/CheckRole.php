<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param string $role
     * @return mixed
     */
    public function handle($request, Closure $next, string $role)
    {
        if (empty($role) || !is_string($role))
            abort(500, 'UPS: undefined roles!');

        $roles = array_map(function ($value) {
            return trim($value);
        }, explode('|', $role));

        /** @var User $user */
        $user = Auth::user();

        if (!$user->hasAnyRole($roles))
            abort(419);

        return $next($request);
    }
}
