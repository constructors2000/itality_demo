<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-06-26
 * Time: 17:12
 */
?>

<div class="table-responsive">
    <table class="table table-bordered">
        <thead class="p-3 mb-2 bg-dark text-white">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Display Name</th>
            <th scope="col">API Key</th>
            <th scope="col">Products</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">#</th>
            <td>{{$vendor->display_name }}</td>
            <td>{{$vendor->api_key }}</td>
            <td>
                <a href="http://amoritalia.biz/b1bplatform/api/v2/download/vendor/csv_temp?api_key={{$vendor->api_key }}"
                   class="btn btn-info btn-sm text-white rounded-0">Download csv</a></td>
        </tr>
        </tbody>
    </table>
</div>

