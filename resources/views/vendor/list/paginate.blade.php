<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-04-12
 * Time: 11:29
 */
?>


<div class="container">
    <div id="accordion">
        @foreach($vendors as $vendor)
            <div class="card rounded-0">
                <div class="card-header rounded-0" id="heading_{{$vendor->user_id }}">
                    <h3 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse"
                                data-target="#collapse_{{$vendor->user_id}}"
                                aria-expanded="true" aria-controls="collapse_{{$vendor->user_id }}">
                            {{$vendor->display_name }}
                        </button>
                    </h3>
                </div>

                <div id="collapse_{{$vendor->user_id }}" class="collapse"
                     aria-labelledby="heading_{{$vendor->user_id }}" data-parent="#accordion">
                    <div class="card-body">
                            @include('vendor.list.table_data')
                            @include('vendor.list.table_api')
                            @include('vendor.list.remote_shop')
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

