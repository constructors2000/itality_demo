<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-06-26
 * Time: 17:14
 */
?>

<div class="alert alert-primary rounded-0" role="alert">Remote Shop</div>
<div class="table-responsive remote-shop">
    @include('vendor.list.table_remote')
</div>

<div class="row">
    <div class="col-sm-12 col-md-3 col-xl-3 col-lg-3">
        <div class="nav flex-column nav-pills" id="v-pills-tab-{{$vendor->user_id }}" role="tablist"
             aria-orientation="vertical">
            <a class="nav-link active" id="v-pills-shopify-{{$vendor->user_id }}-tab" data-toggle="pill"
               href="#v-pills-shopify-{{$vendor->user_id }}" role="tab"
               aria-controls="v-pills-shopify-{{$vendor->user_id }}" aria-selected="true">Shopify</a>
            <a class="nav-link" id="v-pills-bigCommerce-{{$vendor->user_id }}-tab" data-toggle="pill"
               href="#v-pills-bigCommerce-{{$vendor->user_id }}" role="tab"
               aria-controls="v-pills-bigCommerce-{{$vendor->user_id }}" aria-selected="false">BigCommerce</a>
            <a class="nav-link" id="v-pills-wooCommerce-{{$vendor->user_id }}-tab" data-toggle="pill"
               href="#v-pills-wooCommerce-{{$vendor->user_id }}" role="tab"
               aria-controls="v-pills-wooCommerce-{{$vendor->user_id }}" aria-selected="false">WooCommerce</a>
        </div>
    </div>
    <div class="col-sm-12 col-md-9 col-xl-9 col-lg-9">
        <div class="tab-content" id="v-pills-tab-{{$vendor->user_id }}Content">
            <!-- Shopify form -->
            <div class="tab-pane fade show active" id="v-pills-shopify-{{$vendor->user_id }}" role="tabpanel"
                 aria-labelledby="v-pills-shopify-{{$vendor->user_id }}-tab">
                <form class="form-shopify form-shop">

                    <input type="hidden" class="form-control type-shop" name="type" value="shopify">
                    <div class="form-group">
                        <label>Link address
                            <input type="text" class="form-control link-shop" name="link"
                                   placeholder="********.myshopify.com">
                            <small class="form-text text-muted">Enter link to your shop
                                Example:(aefrit4.myshopify.com)
                            </small>
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Api Key
                            <input type="text" class="form-control key" name="key" placeholder="Api Key">
                            <small class="form-text text-muted">Enter Api key By Your shop
                                Example:(52a9c45mf8k530p52cd761bd1fc32a31)
                            </small>
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Password
                            <input type="password" class="form-control password" name="password"
                                   placeholder="Password">
                            <small class="form-text text-muted">Enter Password BY Your shop
                                Example:(d3tg7n69q3f347k9q2e57b8gf4hz85tt)
                            </small>
                        </label>
                    </div>
                    <div class="form-inline col-md-6 col-lg-6 col-xl-6 p-0">
                        <div class="col-6 p-0">Use Collection by Shop</div>
                        <div class="col-6">
                            <input type="checkbox" name="collection" data-toggle="toggle" data-on="Auto"
                                   data-off="Manual" data-onstyle="primary" data-offstyle="warning"
                                   data-width="100" checked>
                        </div>
                    </div>
                    <br/>
                    <div class="form-inline col-md-6 col-lg-6 col-xl-6 p-0">
                        <div class="col-6 p-0">Currency</div>
                        <div class="col-6">
                            <select class="rounded-0 js-select2 form-control" name="currency">
                                <option value="EUR" selected>EUR</option>
                                <option value="USD">USD</option>
                            </select></div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <!-- Bigcomerce form -->
            <div class="tab-pane fade" id="v-pills-bigCommerce-{{$vendor->user_id }}" role="tabpanel"
                 aria-labelledby="v-pills-bigCommerce-{{$vendor->user_id }}-tab">
                <form class="form-bigcommerce form-shop">
                    <input type="hidden" class="form-control type-shop" name="type" value="bigcommerce">
                    <div class="form-group">
                        <label>Store Hash
                            <input type="text" class="form-control link-shop" name="link" placeholder="store hash">
                            <small class="form-text text-muted">Enter store hash
                                Example:(ft5ekyibe4)
                            </small>
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Client Id
                            <input type="text" class="form-control key" name="key" placeholder="Client Id">
                            <small class="form-text text-muted">Enter Client Id By Your shop
                                Example:(4et3ebyjio7nm4xrh41hnumopbdqgoa)
                            </small>
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Auth Token
                            <input type="password" class="form-control password" name="password"
                                   placeholder="Auth Token">
                            <small class="form-text text-muted">EAuth Token BY Your shop
                                Example:(ef45g3vbnj6g6fxgwmfdflsff4j8rgk)
                            </small>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <!-- Woocomerce form -->
            <div class="tab-pane fade" id="v-pills-wooCommerce-{{$vendor->user_id }}" role="tabpanel"
                 aria-labelledby="v-pills-wooCommerce-{{$vendor->user_id }}-tab">
                <form class="form-woocommerce form-shop" method="post">
                    <input type="hidden" class="form-control type-shop" name="type" value="woocommerce">
                    <div class="form-group">
                        <label>Link address
                            <input type="text" class="form-control link-shop" name="link"
                                   placeholder="Link address">
                            <small class="form-text text-muted">Enter link to your shop
                                Example:(www.amoritalia.com)
                            </small>
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Consumer Key
                            <input type="text" class="form-control key" name="key" placeholder="ApiKey">
                            <small class="form-text text-muted">Enter Api key By Your shop
                                Example:(ck_34c6fd9e5g51cd57r261fb8f53r76baa16af596e)
                            </small>
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Consumer Secret
                            <input type="password" class="form-control password" name="password"
                                   placeholder="Password">
                            <small class="form-text text-muted">Enter Password BY Your shop
                                Example:(cs_1brf898aff268402fcg0468ed404ca969cfb6c26)
                            </small>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    <script>
        /* THIS SECTION FOR USER PAGINATION */
        $(document).ready(function () {
            initRemoteForm();
        });

        function initRemoteForm() {

            /**
             * Form submit
             * */
            $('.form-shop').submit(function (e) {
                e.preventDefault();
                e.stopPropagation();

                let $this = $(this);

                $.ajax({
                    url: "{!! route('vendor.remote.shop') !!}",
                    type: "POST",
                    dataType: "html",
                    data: $(this).serialize(),
                    success: function (response) {

                    },
                    error: function (response) {

                    }
                });

            });
        };
    </script>
@endsection
