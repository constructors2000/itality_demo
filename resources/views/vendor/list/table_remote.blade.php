<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-06-26
 * Time: 18:55
 */
?>

<div class="table-responsive">
    <table class="table table-bordered">
        <thead class="p-3 mb-2 bg-dark text-white">
        <tr>
            <th scope="col">Shop</th>
            <th scope="col">Link Address</th>
            <th scope="col">Options</th>
            <th scope="col">action</th>
        </tr>
        </thead>
        <tbody>

        @if(!empty($vendor->remoteShop))
            @foreach($vendor->remoteShop as $remote_shop)
                <tr>
                    <td>{{$remote_shop->type}}({{$remote_shop->currency}})</td>
                    <td>{{$remote_shop->options['ShopUrl']}}</td>
                    <td class="p-0">
                        <ul class="list-group">
                            @foreach($remote_shop->options as $key=>$option)
                                <li class="list-group-item">
                                    <span class="badge badge-primary badge-pill">{{$key}}</span>
                                    {{$option}}
                                </li>
                            @endforeach
                        </ul>
                    </td>
                    <td><a href="#">delete</a>/<a href="#">update</a></td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
