<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-06-26
 * Time: 17:12
 */

?>

<div class="table-responsive">
    <table class="table text-center table-bordered">
        <thead class="p-3 mb-2 bg-dark text-white">
        <tr>
            <th scope="row" class="bg-info " colspan="5">API Methods</th>
        </tr>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Method</th>
            <th scope="col">Count product on pages</th>
            <th scope="col">Type</th>
            <th scope="col">Links</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">#</th>
            <th scope="row">GET</th>
            <td>Paginate product</td>
            <td>JSON</td>
            <td><a target="_blank"
                   href="http://amoritalia.biz/b2bplatform/api/v2/json/getProducts?api_key={{$vendor->api_key }}">http://amoritalia.biz/b2bplatform/api/v2/json/getProducts?api_key={{$vendor->api_key }}</a>
            </td>
        </tr>

        <tr>
            <th scope="row">#</th>
            <th scope="row">GET</th>
            <td>All</td>
            <td>XML</td>
            <td><a target="_blank"
                   href="http://amoritalia.biz/b2bplatform/api/v2/xml/getProducts?api_key={{$vendor->api_key }}">http://amoritalia.biz/b2bplatform/api/v2/xml/getProducts?api_key={{$vendor->api_key }}</a>
            </td>
        </tr>
        </tbody>
    </table>
</div>