@extends('layouts.app')

@section('content')
    <main>
        <div class="container d-flex h-100 justify-content-center">
            <div class="row justify-content-center align-self-center w-100">
                <div class="col-md-8">
                    <div class="card rounded-0 text-center">
                        <div class="card-header text-white bg-dark rounded-0">
                            <h5 class="">Notification</h5>
                        </div>
                        <div class="card-body ">
                            <p class="card-text">{{ __(empty($message)? '' : $message )}}</p>
                            <a href="{{empty($route)? url(config('app.url')) : route($route) }}"
                               class="btn btn-primary float-center rounded-0">Next</a>
                        </div>

                        <div class="card-footer text-muted" id="time">
                            {{empty($time) ? '-': $time}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection



@push('scripts')

    @if(!empty($time))
        <script>
            $(document).ready(function () {
                reset = function () {
                    window.location.replace('{{empty($route)? url(config('app.url')) : route($route) }}');
                };

                var sec = '{!! $time !!}';
                var i = sec;

                setTimeout(function run() {

                    if (i <= 1) {reset();}
                    i = i - 1;
                    $('#time').html(i);
                    setTimeout(run, 1000);
                }, 1000);
            });
        </script>
    @endif
@endpush
