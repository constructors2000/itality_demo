<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-04-12
 * Time: 18:15
 */
?>

@extends('layouts.app')

@section('content')

    @push('modal_1')
        @include('admin.crate.user', [$accounts_supplier, $accounts_vendor])
    @endpush

    <div class="col-lg-12">
        <div class="row justify-content-center">
            <div class="col">

                <div class="alert alert-light rounded-0" role="alert">
                    <a class="btn btn-dark text-white card-link rounded-0"
                       href="#" data-toggle="modal" data-target="#modal_1">Create User</a>
                </div>

                <div id="user_paginator">

                    @include('admin.list.user_pagination')

                    @section('scripts')
                        @parent
                        <script>
                            /* THIS SECTION FOR USER PAGINATION */
                            $(document).ready(function () {
                                initUsersPaginate();
                            });

                            function initUsersPaginate() {

                                /**
                                * Update user roles and accounts
                                * */
                                $('table .js-select2, table tr :checkbox').on('select2:select, select2:unselect, change', function (e) {
                                    e.preventDefault();
                                    e.stopPropagation();

                                    let $this = $(this);
                                    let tr = $(this).parents('tr');
                                    let link = tr.data('href');

                                    values = tr.find(" :input").serializeArray();

                                    /* Because serializeArray() ignores unset checkboxes and radio buttons: */
                                    values = values.concat(
                                        tr.find('input[type=checkbox]:not(:checked)').map(
                                            function() {
                                                return {"name": this.name, "value": 'off'}
                                            }).get()
                                    );

                                    $.ajax({
                                        url: link,
                                        method: "PATCH",
                                        data: values,
                                        cache: false,
                                    }).done(function (data) {
                                        tr.find('.update-user-submit').removeClass('bg-info').addClass('bg-secondary');
                                        toastr.success('Have fun Update!');
                                    }).fail(function (response) {
                                        toastr.error('User not Update');
                                        if (response && response.responseJSON && response.responseJSON.errors) {
                                            const errors = response.responseJSON.errors;

                                            for (let item in errors) {
                                                let _item = item.replace(/\.[^\.]+$/, '')
                                                var input = $(tr).find("#" + _item + "_" + $($this).data("user"));
                                                if (!input.hasClass('form-control')) {
                                                    input = input.parent('.form-control');
                                                }
                                                $(input).addClass('is-invalid', 2000);
                                                $(input).siblings(".invalid-feedback").append('<strong>' + errors[item][0] + '</strong>');

                                                toastr.error(errors[item][0]);
                                            }

                                            setTimeout(function () {
                                                $('.is-invalid').siblings(".invalid-feedback").empty();
                                                $('.is-invalid').removeClass('is-invalid', 2000);
                                            }, 4000);
                                        }
                                    });
                                });

                                /**
                                * Delete user submit
                                * */
                                $('.delete-user-submit').on('click', function (e) {
                                    e.preventDefault();
                                    e.stopPropagation();

                                    if(!confirm("are you want delete user ?"))
                                        return;

                                    let $this = $(this);
                                    let link = $this.attr('href');
                                    let tr = $(this).parents('tr');
                                    console.log(link);
                                    $.ajax({
                                        url: link,
                                        method: "DELETE",
                                        cache: false,
                                    }).done(function (data) {
                                        tr.remove();
                                        toastr.warning('User remove');
                                    }).fail(function (response) {
                                        toastr.error('User not remove');
                                    });

                                });
                            };

                        </script>
                    @endsection
                </div>
            </div>
        </div>
    </div>
@endsection






