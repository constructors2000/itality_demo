<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-04-14
 * Time: 19:18
 */
?>


<div class="card rounded-0">
    <div class="card-header bg-dark text-white rounded-0">{{ __('Register') }}</div>

    <div class="card-body">
        <form method="POST" id="create_user" action="{{route('admin.registerUser')}}">
            @csrf
            {{-- NAME --}}
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                <div class="col-md-8">
                    <input id="name" type="text"
                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} rounded-0"
                           name="name" value="{{ old('name') }}" required autofocus>

                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                </div>
            </div>

            {{-- NAME --}}
            <div class="form-group row">
                <label for="email"
                       class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                <div class="col-md-8">
                    <input id="email" type="email"
                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} rounded-0"
                           name="email" value="{{ old('email') }}" required>

                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>

                </div>
            </div>

            {{-- PASSWORD --}}
            <div class="form-group row">
                <label for="password"
                       class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                <div class="col-md-8">
                    <input id="password" type="password"
                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} rounded-0"
                           name="password" required>

                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>

                </div>
            </div>

            {{-- CONFIRM PASSWORD --}}
            <div class="form-group row">
                <label for="password-confirm"
                       class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                <div class="col-md-8">
                    <input id="password-confirm" type="password" class="form-control rounded-0"
                           name="password_confirmation" required>
                </div>
            </div>

            {{-- ACCOUNTS SUPPLIER --}}
            @if(!empty($accounts_supplier))
                <div class="form-group row">
                    <label for="accounts_supplier"
                           class="col-md-4 col-form-label text-md-right">Accounts Supplier</label>
                    <div class="col-md-8 border-0  {{ $errors->has('accounts') ? ' is-invalid' : '' }}">
                        <select class="rounded-0 custom-select js-select2 form-control"
                                name="accounts_supplier[]"
                                multiple="multiple" id="accounts_supplier">
                            @foreach($accounts_supplier as $account)
                                <option value="{{$account->{'account.id'} }}">{{$account->{'account.name'} }}</option>
                            @endforeach
                        </select>
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('accounts') }}</strong>
                        </span>
                    </div>
                </div>
            @endif

            {{-- ACCOUNTS VENDORS --}}
            @if(!empty($accounts_vendor))
                <div class="form-group row">
                    <label for="accounts_vendor"
                           class="col-md-4 col-form-label text-md-right">Accounts Vendor</label>
                    <div class="col-md-8 border-0  {{ $errors->has('accounts') ? ' is-invalid' : '' }}">
                        <select class="rounded-0 custom-select js-select2 form-control"
                                name="accounts_vendor[]"
                                multiple="multiple" id="accounts_vendor">
                            @foreach($accounts_vendor as $account)
                                <option value="{{$account->{'account.id'} }}">{{$account->{'account.name'} }}</option>
                            @endforeach
                        </select>
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('accounts') }}</strong>
                        </span>
                    </div>
                </div>
            @endif

            {{-- ROLES --}}
            <div class="form-group row">
                <label for="roles"
                       class="col-md-4 col-form-label text-md-right">Roles</label>
                <div class="col-md-8 border-0 {{ $errors->has('roles') ? ' is-invalid' : '' }}">
                    <select class="rounded-0 custom-select js-select2 form-control"
                            name="roles[]" multiple="multiple" id="roles">
                        @foreach(['vendor', 'supplier', 'admin'] as $role)
                            <option value="{{$role}}">
                                {{ucfirst($role) }}
                            </option>
                        @endforeach
                    </select>
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('roles') }}</strong>
                    </span>
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary rounded-0">
                        {{ __('Register') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>


@section('scripts')
    @parent
    <script>
        /* INIT CREATE USER SECTION */
        $(document).ready(function () {
            initCreateUser();
        });
        function initCreateUser() {
            $('#create_user').on('submit', function (e) {
                e.preventDefault();
                e.stopPropagation();

                let form = $(this);
                let link = form.attr('action');
                let method = form.attr('method');

                $.ajax({
                    url: link,
                    method: method,
                    data: form.serialize(),
                    cache: false,
                }).done(function (data) {
                    if (data.html && data.data) {
                        $('#user_paginator table tbody').append(data.html);
                        let selector = '#user_paginator #user_' + data.data.id + ' .js-select2';
                        $(selector).select2({width: "100%"});

                        $(selector).trigger('change');

                        toastr.success('Create new User!');

                        form.reset();

                    }
                }).fail(function (response) {
                    if (response && response.responseJSON && response.responseJSON.errors) {
                        const errors = response.responseJSON.errors;
                        console.log(errors);
                        for (let item in errors) {
                            let _item = item.replace(/\.[^\.]+$/, '')
                            var input = $(form).find("#" + _item);
                            if (!input.hasClass('form-control')) {
                                input = input.parent('.form-control');
                            }
                            $(input).addClass('is-invalid', 2000);
                            $(input).siblings(".invalid-feedback").append('<strong>' + errors[item][0] + '</strong>');
                            toastr.error(errors[item][0]);
                        }

                        setTimeout(function () {
                            $('.is-invalid').siblings(".invalid-feedback").empty();
                            $('.is-invalid').removeClass('is-invalid', 2000);
                        }, 4000);
                    }
                });
            });
        };
    </script>
@endsection
