<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-04-14
 * Time: 19:26
 */
?>


<div class="table-responsive">
    <table class="table">
        <thead class="thead-dark">
        <tr class="">
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Accounts Supplier</th>
            <th scope="col">Accounts Vendor</th>
            <th scope="col-3">Roles</th>
            <th scope="col">Enable</th>
            <th scope="col">Event</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            @include('admin.list.user_row', [$user, $accounts_supplier, $accounts_vendor])
        @endforeach
        </tbody>
    </table>
    {{ $users->links() }}
</div>

