<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-04-23
 * Time: 13:29
 */
?>

<tr id="user_{{$user->id}}" class=" @if($user->hasRole('admin')) bg-warning
                            @elseif($user->hasRole('supplier')) bg-success
                            @elseif($user->hasRole('vendor')) bg-info
                            @endif" data-user="{{$user->id}}" data-href="{{route('admin.updateUser', [$user->id])}}">
    <td>{{$user->id}}</td>
    <td>{{$user->name}}</td>
    <td>{{$user->email}}</td>

    <td>
        <select class="rounded-0 custom-select js-select2 form-control"
                name="accounts_supplier[]"
                multiple="multiple" id="accounts_supplier_{{$user->id}}">
            @foreach($accounts_supplier as $account)
                <option value="{{$account->user_id }}"
                        @if(!empty($user->setting) && in_array($account->user_id, $user->setting->accounts_supplier))  selected @endif >
                    {{$account->display_name }}
                </option>
            @endforeach
        </select>
        <span class="invalid-feedback" role="alert"><strong></strong></span>
    </td>

    <td>
        <select class="rounded-0 custom-select js-select2 form-control"
                name="accounts_vendor[]"
                multiple="multiple" id="accounts_vendor_{{$user->id}}">
            @foreach($accounts_vendor as $account)
                <option value="{{$account->user_id }}"
                        @if(!empty($user->setting) && in_array($account->user_id, $user->setting->accounts_vendor)) selected @endif >
                    {{$account->display_name }}
                </option>
            @endforeach
        </select>
        <span class="invalid-feedback" role="alert"><strong></strong></span>
    </td>

    <td >
        <select class="rounded-0 custom-select js-select2 form-control"
                name="roles[]"
                multiple="multiple" id="roles_{{$user->id}}">
            @foreach(\App\Role::available_roles as $role)
                <option value="{{$role}}"
                        @if(!empty($user->roles) && $user->roles->contains('name', $role)) selected @endif >
                    {{ucfirst($role) }}
                </option>
            @endforeach
        </select>
        <span class="invalid-feedback" role="alert"><strong></strong></span>
    </td>

    <td>
        <input type="checkbox" name="user_verified" @if(!empty($user->user_verified_at)) checked @endif data-toggle="toggle" data-size="sm">
    </td>

    <td>

        <div class="btn-group-vertical btn-group-toggle" data-toggle="buttons">
            <a class="btn bg-danger text-white border border-secondary btn-sm  rounded-0 delete-user-submit"
               href="{{route('admin.deleteUser', [$user->id])}}"
               data-user="{{$user->id}}">{{ __('Delete') }}</a>
        </div>
    </td>
</tr>
