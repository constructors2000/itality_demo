<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-04-08
 * Time: 01:39
 */
?>

@extends('layouts.app')

{{-- main section --}}
@section('content')
    {{-- if filter exist it include in "product.list.paginate" --}}
    @if(!empty($filter))
        @include('product.filter')
    @endif
    {{-- list of poroducts --}}
    @include('product.list.paginate')
@endsection
