<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-04-08
 * Time: 01:42
 */
?>

@section('head')
    @parent
    <style>
        .select2-selection.select2-selection--single{
            min-height: 36px;
        }
        .select2-selection.select2-selection--single .select2-selection__rendered{
            padding: 2px 2px 4px 5px;
        }
    </style>
@endsection

@section('filter')
    <div class="card rounded-0">
        <div class="card-header bg-secondary rounded-0" id="headingFilter">
            <button class="btn btn-outline-info rounded-0"
                    data-toggle="collapse"
                    data-target="#collapseFilter"
                    aria-expanded="true"
                    aria-controls="collapseFilter">
                Filter
            </button>
        </div>

        <div id="collapseFilter" class="collapse show" aria-labelledby="headingFilter">
            <div class="card-body">
                <form class="needs-validation" id="filter" action="{{route('product.paginate')}}" method="POST"
                      novalidate>
                    <div class="form-row">
                        <!-- GROUPS -->
                        @if(!empty($filter['groups']))
                            <div class="col-md-3 mb-3">
                                <label for="filter_groups">Product group</label>
                                <select class="form-control rounded-0 custom-select js-select2"
                                        data-placeholder="Select groups"
                                        name="filter_groups[]"
                                        multiple="multiple"
                                        id="filter_groups">
                                    @foreach($filter['groups'] as $group)
                                        <option value="{{$group->id}}"
                                                @if(@in_array($group->id, app('request')->input('filter_groups') ?: [])) selected @endif>
                                            {{$group->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                    <!-- CATEGORIES -->
                        @if(!empty($filter['categories']))
                            <div class="col-md-3 mb-3">
                                <label for="filter_categories">Categories</label>
                                <select class="form-control rounded-0 custom-select js-select2"
                                        data-placeholder="Select categories"
                                        name="filter_categories[]"
                                        multiple="multiple"
                                        id="filter_categories">
                                    @foreach($filter['categories'] as $category)
                                        <option value="{{$category->id}}"
                                                @if(@in_array($category->id, app('request')->input('filter_categories')?: [])) selected @endif>
                                            {{$category->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                    <!-- SUB CATEGORIES -->
                        @if(!empty($filter['sub_categories']))
                            <div class="col-md-3 mb-3">
                                <label for="filter_sub_categories">Sub Categories</label>
                                <select class="form-control rounded-0 custom-select js-select2"
                                        data-placeholder="Select sub category"
                                        name="filter_sub_categories[]"
                                        multiple="multiple"
                                        id="filter_sub_categories">
                                    @foreach($filter['sub_categories'] as $sub_category)
                                        <option value="{{$sub_category->id}}"
                                                @if(@in_array($sub_category->id, app('request')->input('filter_sub_categories')?: [])) selected @endif>
                                            {{$sub_category->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                    <!-- BRANDS -->
                        @if(!empty($filter['brands']))
                            <div class="col-md-3 mb-3">
                                <label for="filter_brands">Brands</label>
                                <select class="form-control rounded-0 custom-select js-select2"
                                        data-placeholder="Select brands"
                                        name="filter_brands[]"
                                        multiple="multiple"
                                        id="filter_brands">
                                    @foreach($filter['brands'] as $brand)
                                        <option value="{{$brand->name}}"
                                                @if(@in_array($brand->id, app('request')->input('filter_brands')?:[])) selected @endif>
                                            {{$brand->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                    </div>

                    <div class="form-row">
                        <!-- SUPPLIER -->
                        @if(!empty($filter['suppliers']) && Auth::user()->hasAnyRole(['admin', 'supplier']))
                            <div class="col-md-4 mb-3">
                                <label for="filter_suppliers">Supplier</label>
                                <select class="form-control rounded-0 custom-select js-select2"
                                        data-placeholder="Select suppliers"
                                        name="filter_suppliers[]"
                                        multiple="multiple"
                                        id="filter_suppliers">
                                    @foreach($filter['suppliers'] as $supplier)
                                        <option value="{{ $supplier->{'id'} }}"
                                                @if(@in_array($supplier->{'id'}, app('request')->input('filter_suppliers')?:[])) selected @endif>
                                            {{ $supplier->{'name'} }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                    <!-- VENDOR -->
                        @if(!empty($filter['vendors']))
                            <div class="col-md-4 mb-3">
                                <label for="filter_vendors">Vendors</label>
                                <select class="form-control rounded-0 custom-select js-select2"
                                        data-placeholder="Select vendor's"
                                        name="filter_vendors[]"
                                        multiple="multiple"
                                        id="filter_vendors">
                                    @foreach($filter['vendors'] as $vendors)
                                        <option value="{{ $vendors->{'id'} }}"
                                                @if(@in_array($vendors->{'id'}, app('request')->input('filter_vendors')?:[])) selected @endif>
                                            {{ $vendors->{'name'} }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                    @endif
                    <!-- ACTIVE -->
                        <div class="col-md-4 mb-3">
                            <label for="filter_active">Status</label>
                            <select class="form-control rounded-0 custom-select js-select2"
                                    name="filter_active"
                                    id="filter_active">
                                @foreach(['Any'=> (-1), 'Active'=>0, 'InActive'=>1] as $active=>$val)
                                    <option value="{{$val}}"
                                            @if($val == (-1) && is_null(app('request')->input('filter_active')))
                                                selected
                                            @elseif( is_integer(app('request')->input('filter_active')) &&  $val === (int)app('request')->input('filter_active'))
                                                selected
                                            @endif>
                                        {{$active}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <!-- PRODUCT ID  -->
                        <div class="col-md-3 mb-3">
                            <label for="filter_product_id">Product ID</label>
                            <input type="text" class="form-control rounded-0 filter-string"
                                   id="filter_product_id"
                                   name="filter_product_id"
                                   value="{{app('request')->input('filter_product_id') ?: ''}}"
                                   placeholder="Product ID">
                        </div>
                        @if(!empty($filter['suppliers']) && Auth::user()->hasAnyRole(['admin', 'supplier']))
                        <div class="col-md-3 mb-3">
                            <!-- PRODUCT ID (supplier) -->
                            <label for="filter_product_supplier_id">Product ID (supplier)</label>
                            <input type="text" class="form-control rounded-0 filter-string"
                                   id="filter_product_supplier_id"
                                   name="filter_product_supplier_id"
                                   value="{{app('request')->input('filter_product_supplier_id') ?: ''}}"
                                   placeholder="Product Supplier ID">
                        </div>
                        @endif
                        <div class="col-md-3 mb-3">
                            <!-- PRODUCT CODE -->
                            <label for="filter_product_code">Product Code</label>
                            <input type="text" class="form-control rounded-0 filter-string"
                                   id="filter_product_code"
                                   name="filter_product_code"
                                   value="{{app('request')->input('filter_product_code') ?: ''}}"
                                   placeholder="Product Code">
                        </div>
                        <div class="col-md-3 mb-3">
                            <!-- PRODUCT NAME -->
                            <label for="filter_product_name">Product name</label>
                            <input type="text" class="form-control rounded-0 filter-string"
                                   id="filter_product_name"
                                   name="filter_product_name"
                                   value="{{app('request')->input('filter_product_name') ?: ''}}"
                                   placeholder="Product name">
                        </div>
                    </div>

                    <div class="form-row">
                        <!-- EXIST IMAGE -->
                        <div class="col-md-3 mb-3">
                            <label for="filter_image">Images</label>
                            <select class="form-control rounded-0 custom-select js-select2"
                                    name="filter_image"
                                    id="filter_image">
                                @foreach(['Any'=> (-1), 'Exist'=>0, 'Empty'=>1] as $active=>$val)
                                    <option value="{{$val}}"
                                            @if($val == (-1) && is_null(app('request')->input('filter_image')))
                                            selected
                                            @elseif(is_integer(app('request')->input('filter_image')) &&  $val === (int)app('request')->input('filter_image'))
                                            selected
                                        @endif>
                                        {{$active}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <!-- PRODUCT COLOR
                        <div class="col-md-3 mb-3">
                            <label for="filter_colors">Product Color</label>
                            <select class="form-control rounded-0 custom-select js-select2"
                                    name="filter_colors[]"
                                    multiple="multiple" id="filter_colors">
                                @foreach(['Red'=>'red', 'Black'=>'black'] as $active=>$val)
                                    <option value="{{$val}}"
                                            @if(@in_array($val, app('request')->input('filter_colors'))) selected @endif>
                                        {{$active}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        -->
                        <!-- MADE IN -->
                        <div class="col-md-3 mb-3">
                            <label for="filter_made">Made in </label>
                            <select class="form-control rounded-0 custom-select js-select2"
                                    data-placeholder="Select countries"
                                    name="filter_made[]"
                                    multiple="multiple"
                                    id="filter_made">
                                @foreach(['Italy'=>'italy', 'Japan'=>'japan'] as $made=>$val)
                                    <option value="{{ $val }}"
                                            @if(@in_array($val, app('request')->input('filter_made')?:[])) selected @endif>
                                        {{ $made }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button class="btn btn-primary rounded-0" type="submit">Submit Filter <i class="fas fa-filter"></i>
                    </button>
                </form>
            </div>
        </div>
    </div>
    <hr/>
@endsection

@section('scripts')
    @parent
    <script>
        $(document).ready(function () {
            init();
        });

        function init() {
            /**
             * Init select2
             * */
            $('.custom-select').on('select2:unselect select2:select', function (e) {
                clearQuery(this.name, e.params.data.id)
            });

            /**
             * Event when text update in input
             * */
            $('.filter-string').focusout(function (e) {
                let url = UpdateQueryString(this.name, this.value);
                if (isEmpty(this.value))
                    url = url.replace(this.name + '=', '');
                window.history.pushState("", "", url);
            });

            /**
             * Use filter for search
             * */
            $('body').on('submit', 'form#filter', function (e) {
                e.stopPropagation();
                e.preventDefault();

                let $data = $(this).serializeArray();

                let _url = window.location.href;
                var url = null;

                $data.forEach(function ($input) {
                    if (isEmpty($input.value) ||
                        _url.includes($input.name + '[]=' + $input.value) ||
                        _url.includes($input.name + '=' + $input.value)) return

                    url = UpdateQueryString($input.name, $input.value, url);
                });

                if (!url) url = _url;

                url = url.replace('?&', '?').replace('&&', '&');
                url = decodeURIComponent(url);
                url = UpdateQueryString('page', 1, url)

                window.history.pushState("", "", url);
                getArticles(url);
            });
        }

        /**
         * Ajax for update list of products
         * */
        function getArticles(url) {
            $.ajax({
                url: url,
                cache: false,
            }).done(function (data) {
                $('#product_paginate').html(data);
            }).fail(function () {
                alert('Articles could not be loaded.');
            });
        }

        /**
         * This function for change or add query to url
         * */
        function UpdateQueryString(key, value, url) {
            if (!url) url = window.location.href;
            var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
                hash;

            if (re.test(url)) {
                if (typeof value !== 'undefined' && value !== null)
                    return url.replace(re, '$1' + key + "=" + value + '$2$3');
                else {
                    hash = url.split('#');
                    url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
                    if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                        url += '#' + hash[1];
                    return url;
                }
            } else {
                if (typeof value !== 'undefined' && value !== null) {
                    var separator = url.indexOf('?') !== -1 ? '&' : '?';
                    hash = url.split('#');
                    url = hash[0] + separator + key + '=' + value;
                    if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                        url += '#' + hash[1];
                    return url;
                } else
                    return url;
            }
        }

        /**
         * delete parameters from url
         * */
        function clearQuery(name, id) {
            var url = window.location.href;

            url = decodeURIComponent(url);
            url = url.replace(/(\[.*?\]|\(.*?\)) */g, "[]");

            url = url.replace(name + '=' + id, '');

            window.history.pushState("", "", url);
        }

        /**
         * check if string empty
         * */
        function isEmpty(str) {
            return (!str || 0 === str.length);
        }

    </script>
@endsection



