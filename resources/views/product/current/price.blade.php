<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-04-12
 * Time: 14:12
 */
?>

@if($price && count($price)>0)
    <div class="table-responsive">
        <table class="table table-sm">
            <caption>Price for vendors</caption>
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Vendor</th>
                <th scope="col">Itality Selling</th>
                <th scope="col">Vendor RRP</th>
                <th scope="col">Net Margin</th>
            </tr>
            </thead>
            @if(Auth::user()->hasAnyRole(['admin', 'supplier', 'vendor']))
                <tbody>
                @foreach($price as $item=>$value)
                    <tr>
                        <th scope="row">{{$item}}</th>
                        <td>{{$value->{'supplier.name'} }}</td>
                        <td>{{$value->{'price.itality_selling'} }}</td>
                        <td>{{$value->{'price.vendor_rrp'} }}</td>
                        <td>{{$value->{'price.net_margin'} }}</td>
                    </tr>
                @endforeach
                </tbody>
            @endif
        </table>
    </div>
@endif