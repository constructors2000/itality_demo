@php
    $user = \Illuminate\Support\Facades\Auth::user();
    $options = isset($user->setting->options) ? $user->setting->options : [];
    $container = \App\Setting::PAGINATION['0'];
    if(!empty($options['product_pagination']))
        $container = $options['product_pagination'];


@endphp
<div class="m-3">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card rounded-0">
                <div class="card-header d-flex text-white bg-dark rounded-0">
                    <div class="mr-auto">Products</div>
                    <div class="ml-auto">
                        <div class="row">
                            <select name="per_page" id="per_page" class="text-white bg-dark">>
                                @foreach([25,50,100,200,500] as $per_page)
                                    <option value="{{$per_page}}"
                                            @if($per_page == Request::query('per_page')) selected @endif>
                                        {{$per_page}}
                                    </option>
                                @endforeach
                            </select>

                            <div class="ml-3 change-pagination">
                                <a href="{{ route('users::changePagination', [Auth::id(),  \App\Setting::PAGINATION['1']])  }}" id="paginate_table"
                                   class="btn p-0 @if($container ==  \App\Setting::PAGINATION['1']) disabled @else text-white @endif">
                                    <i class="fa fa-list" style="font-size:24px"></i>
                                </a>
                                <a href="{{ route('users::changePagination', [Auth::id(),  \App\Setting::PAGINATION['0']])  }}" id="paginate_card"
                                   class="btn p-0 @if($container ==  \App\Setting::PAGINATION['0']) disabled  @else text-white @endif">
                                    <i class="fas fa-th" style="font-size:24px"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div>
                        @yield('filter')
                    </div>
                    <div id="product_paginate">
                        @include("product.list.{$container}.container", ['products'=>$products])
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@section('scripts')
    @parent
    <script>
        $(document).ready(start);

        /**
         * Init after document ready
         * */
        function start() {

            /**
             * Click event on paginate for update products card throw ajax
             * */
            $('body').on('click', '.pagination a', function (e) {
                e.preventDefault();

                var url = $(this).attr('href');

                url = decodeURIComponent(url);
                url = url.replace(/(\[.*?\]|\(.*?\)) */g, "[]");

                getArticles(url, '#product_paginate');
                window.history.pushState("", "", url);
            });

            /**
             * Click event on change preview product
             * */
            $('body').on('click', '.change-pagination a', function (e) {
                e.preventDefault();
                let $this = $(this);
                var url = $(this).attr('href');
                let main_url = window.location.href;

                let calback = function () {
                    $('.change-pagination a').toggleClass('disabled text-white');
                    getArticles(main_url, '#product_paginate');
                };

                getArticles(url, '', calback);
            });

            /**
             * Change count product
             * */
            $('body').on('change', '#per_page', function (e) {
                var url = UpdateQueryString('per_page', this.value);

                url = decodeURIComponent(url);
                url = url.replace(/(\[.*?\]|\(.*?\)) */g, "[]");

                getArticles(url, '#product_paginate');
            });

            /**
             * Show price
             * */
            $('body').on('click', '.show-price', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                getArticles(url, '#modal_1 .modal-body');

            });

            /**
             * Ajax for update data
             * */
            function getArticles(url, id, calback= null) {
                $.ajax({
                    url: url,
                    cache: false,
                }).done(function (data) {
                    $(id).html(data);
                    if(calback != null){
                        calback(data, id);
                    }
                }).fail(function () {
                    alert('Articles could not be loaded.');
                });
            }

            /**
             * This function for change or add query to url
             * */
            function UpdateQueryString(key, value, url) {
                if (!url) url = window.location.href;
                var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
                    hash;

                if (re.test(url)) {
                    if (typeof value !== 'undefined' && value !== null)
                        return url.replace(re, '$1' + key + "=" + value + '$2$3');
                    else {
                        hash = url.split('#');
                        url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
                        if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                            url += '#' + hash[1];
                        return url;
                    }
                } else {
                    if (typeof value !== 'undefined' && value !== null) {
                        var separator = url.indexOf('?') !== -1 ? '&' : '?';
                        hash = url.split('#');
                        url = hash[0] + separator + key + '=' + value;
                        if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                            url += '#' + hash[1];
                        return url;
                    } else
                        return url;
                }
            }


        };
    </script>

@endsection



