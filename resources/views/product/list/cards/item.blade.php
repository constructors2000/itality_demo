<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-04-11
 * Time: 10:12
 */
?>

@php
    /*
    IT IS NOT GOOD IDEA. BUT IT'S TEMPORARY SOLUTION. BECAUSE MANY PICTURE NOT IN GOOGLE DRIVE
    */


    $imgs = !empty($product->{'product.images'}) ? json_decode($product->{'product.images'}) : [];

    foreach ($imgs as $key=>$img){
        if(!empty($img) && stristr($img->url, 'google') === false) {
                if (($product->{'supplier.id'} == 1)) {
                    $url_arr = explode("/", $img->url);
                    $image_name = $url_arr[2];
                    $img->url = "https://www.brandsdistribution.com/prod" . '/' . $image_name;
                }
                elseif (strpos($img->url, 'www.dropbox.com') !== false) {
                    $img->url = str_replace('dl=0', 'raw=1', $img->url);
                }
                $imgs[$key] = $img;
        }
    }
@endphp

<div class="card  @if($product->{'product.active'} || empty($imgs)) inactive-card @endif">
    <div id="carouselExampleControls_{{ $product->{'product.id'} }}" class="position-relative carousel slide"
         data-ride="carousel" data-interval="false">
        <div class="carousel-inner">
            @if(empty($imgs))
                <div class="card-img-top carousel-item active"> {{-- !WTF active--}}
                    <img class="d-block w-100" src="{{url('img/empty_img.jpg')}}"
                         onerror="this.onerror=null; this.src='{{  url('img/empty_img.jpg')  }}';" alt="Card image cap">
                </div>
            @endif
            @foreach($imgs as $key=>$img)
                <div class="card-img-top carousel-item @if($key <1) active @endif">
                    <img class="d-block w-100" src="{{$img->url}}"
                         onerror="this.onerror=null; this.src='{{  url('img/empty_img.jpg')  }}';" alt="Card image cap">
                </div>
            @endforeach
        </div>
        @if(count($imgs) > 1)

            <a class="carousel-control-prev" href="#carouselExampleControls_{{ $product->{'product.id'} }}"
               role="button"
               data-slide="prev">
                <span class="carousel-control-prev-icon text-light bg-dark rounded-circle" aria-hidden="true"></span>
                <span class="sr-only text-light bg-dark">Previous</span>
            </a>
            <a class="carousel-control-next " href="#carouselExampleControls_{{ $product->{'product.id'} }}"
               role="button"
               data-slide="next">
                <span class="carousel-control-next-icon text-light bg-dark rounded-circle" aria-hidden="true"></span>
                <span class="sr-only text-light bg-dark">Next</span>
            </a>
        @endif

        <div class="card-img-overlay">
            <h6 class="card-title">
                <small class="text-muted">code:</small>
                <small class="text-muted">{{ $product->{'product.code'} }}</small>
            </h6>
            @if(Auth::user()->hasAnyRole(['admin', 'supplier']))
                <h6>
                    <small class="text-muted">Supplier:</small>
                    <small>{{ $product->{'supplier.name'} }}</small>
                </h6>
            @endif
            <p class="card-text"></p>
        </div>
    </div>

    <div class="card-body">
        <h5 class="card-title">
            <a href="#" class="text-info">{{($product->{'product.brand'})}}</a>
            <a href="#" class="text-secondary">{{($product->{'category.name'})}}</a>
            {{($product->{'sub_category.name'})}} {{ $product->{'product.name'} }}
        </h5>
    </div>
    <div class="card-footer d-flex p-2">
        <div class="mr-auto">
            <small class="text-muted"><span class="d-sm-none d-md-inline">Availability</span>
                <span class="badge badge-primary "> {{ $product->{'product.availability'} }} </span>
            </small>
        </div>
        <div class="mr-auto">
            <a class="btn btn-info btn-sm text-white card-link show-price"
               href="{{route('product.current.selling_price', [ $product->{'product.id'} ])}}" data-toggle="modal"
               data-target="#modal_1">price</a>
        </div>
        <div>
            <div class="">
                    <span class="text-muted "> <span class="d-sm-none d-md-inline">id:</span>
                        <span class="badge badge-dark"> {{ $product->{'product.id'} }} </span>
                    </span>
            </div>
        </div>
    </div>
</div>

