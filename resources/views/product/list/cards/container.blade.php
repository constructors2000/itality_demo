<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-04-04
 * Time: 15:35
 */
?>

{{-- hrader pagination for products --}}
{{ $products->links() }}

<div class="card-columns">
    @foreach ($products as $product)
        @include('product.list.cards.item', ['product'=>$product])
    @endforeach
</div>

{{-- footer pagination for products --}}
{{ $products->links() }}