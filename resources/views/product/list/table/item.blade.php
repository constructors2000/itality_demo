<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-05-29
 * Time: 12:09
 */
?>

@php
    /*
    IT IS NOT GOOD IDEA. BUT IT'S TEMPORARY SOLUTION. BECAUSE MANY PICTURE NOT IN GOOGLE DRIVE
    */


    $imgs = !empty($product->{'product.images'}) ? json_decode($product->{'product.images'}) : [];

    foreach ($imgs as $key=>$img){
        if(!empty($img) && stristr($img->url, 'google') === false) {
                if (($product->{'supplier.id'} == 1)) {
                    $url_arr = explode("/", $img->url);
                    $image_name = $url_arr[2];
                    $img->url = "https://www.brandsdistribution.com/prod" . '/' . $image_name;
                }
                elseif (strpos($img->url, 'www.dropbox.com') !== false) {
                    $img->url = str_replace('dl=0', 'raw=1', $img->url);
                }
                $imgs[$key] = $img;
        }
    }
@endphp

<tr class="@if($product->{'product.active'} || empty($imgs)) table-danger text-white @endif">
    <td class="text-center" scope="row">
        {{$product->{'product.id'} }}
    </td>
    <td class="" scope="row">
        <div id="carouselExampleControls_{{ $product->{'product.id'} }}" class="carousel slide"
             style="max-width: 100px;"
             data-ride="carousel" data-interval="false">
            <div class="carousel-inner thumbnail">
                @if(empty($imgs))
                    <div class="carousel-item active">
                        <img class="d-block" src="{{url('img/empty_img.jpg')}}" style="max-width: 100px;"
                             onerror="this.onerror=null; this.src='{{  url('img/empty_img.jpg')  }}';"
                             alt="Card image cap">
                    </div>
                @endif
                @foreach($imgs as $key=>$img)
                    <div class="carousel-item @if($key <1) active @endif">
                        <img class="d-block" src="{{$img->url}}" style="max-width: 100px;"
                             onerror="this.onerror=null; this.src='{{  url('img/empty_img.jpg')  }}';"
                             alt="Card image cap">
                    </div>
                @endforeach
            </div>
            @if(count($imgs) > 1)

                <a class="carousel-control-prev" href="#carouselExampleControls_{{ $product->{'product.id'} }}"
                   role="button"
                   data-slide="prev">
                    <span class="carousel-control-prev-icon text-light bg-dark rounded-circle"
                          aria-hidden="true"></span>
                    <span class="sr-only text-light bg-dark">Previous</span>
                </a>
                <a class="carousel-control-next " href="#carouselExampleControls_{{ $product->{'product.id'} }}"
                   role="button"
                   data-slide="next">
                    <span class="carousel-control-next-icon text-light bg-dark rounded-circle"
                          aria-hidden="true"></span>
                    <span class="sr-only text-light bg-dark">Next</span>
                </a>
            @endif
        </div>
    </td>
    <td class="text-center" scope="row">
        {{ $product->{'product.name'} }}
        <br/>
        <a class="btn btn-info btn-sm text-white card-link show-price"
           href="{{route('product.current.selling_price', [ $product->{'product.id'} ])}}" data-toggle="modal"
           data-target="#modal_1">price</a>
    </td>
    <td class="text-center" scope="row">
        {{ $product->{'product.code'} }}
    </td>
    <td class="text-center" scope="row">
        {{ $product->{'product.brand'} }}
    </td>
    <td class="text-center" scope="row">
        {{ $product->{'category.name'} }}
    </td>
    <td class="text-center" scope="row">
        <span class="badge badge-primary "> {{ $product->{'product.availability'} }} </span>
    </td>
    @if(Auth::user()->hasAnyRole(['admin', 'supplier']))
        <td class="text-center" scope="row">
            {{ $product->{'supplier.name'} }}
        </td>
        <td class="text-center" scope="row">
            {{ $product->{'product.unique_id'} }}
        </td>
    @endif
</tr>
