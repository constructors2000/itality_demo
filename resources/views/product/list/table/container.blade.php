<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-05-29
 * Time: 12:09
 */
?>

{{-- hrader pagination for products --}}
{{ $products->links() }}
<div class="table-responsive">
    <table class="table table-striped table-hover table-sm">
        <thead class="thead-dark">
        <tr>
            <th class="text-center" cope="col">#ID</th>
            <th class="text-center" cope="col">Image</th>
            <th class="text-center" cope="col">Name</th>
            <th class="text-center" cope="col">Code</th>
            <th class="text-center" cope="col">Brand</th>
            <th class="text-center" cope="col">Category</th>
            <th class="text-center" cope="col">Q.ty</th>
            @if(Auth::user()->hasAnyRole(['admin', 'supplier']))
                <th class="text-center" cope="col">Supplier</th>
                <th class="text-center" cope="col">Reference</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach ($products as $product)
            @include('product.list.table.item', ['product'=>$product])
        @endforeach
        </tbody>
    </table>
</div>
{{-- footer pagination for products --}}
{{ $products->links() }}
