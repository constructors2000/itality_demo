<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Amoritalia</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{ url("css/app.css")}}">
    <!-- Styles -->
    <style>
        html, body {
            background-color: #030915;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .itality_text {
            color: white; /* Fallback: assume this color ON TOP of image */
            background: url("img/itality_flag.jpg") -0px -40px ;
            background-size: contain;
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }

        .bg {
            /* The image used */
            background-image: url("img/bg_logo.jpg");

            /* Full height */
            height: 100%;

            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .card-login {
            min-width: 30vw;
        }

        .title {
            font-size: 50px;
        }

        .form-control:focus {
            border-color: #060d1f;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgb(1, 22, 47);
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        @media (min-width: 992px) {
            body {
                overflow: hidden;
            }

            .title {
                font-size: 84px;
            }
        }
    </style>
</head>
<body>

<div class="row justify-content-center bg d-flex justify-content-around">

    <div class="title m-b-md text-white mt-3 itality_text font-weight-bold">
        Amoritalia
    </div>
    <div class="d-flex align-items-center">
        <div class="card border-info rounded-0 card-login">
            <div class="card-header bg-white flex-center"><span>{{ __('Login') }}</span></div>

            <div class="card-body">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group row">
                        <label for="email"
                               class="col-md-4 col-form-label text-md-right">{{ __('Login') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email"
                                   class="form-control rounded-0 {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                   name="email"
                                   value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password"
                               class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password"
                                   class="form-control rounded-0{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                   name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6 offset-md-4">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember"
                                       id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-dark rounded-0">
                                {{ __('Login') }}
                            </button>

                        </div>
                    </div>
                </form>
            </div>

            <div class="card-footer text-muted">
                <p class="text-center m-0"><a href="{{ route('register') }}">{{ __('Become a Partner') }}</a>
                    &nbsp;&nbsp;|&nbsp;&nbsp;
                    @if (Route::has('password.request'))
                        <a  data-toggle="modal" data-target="#modal-reset-password" href="#">{{ __('Lost Password?') }}</a>
                    @endif
                </p>
            </div>
        </div>
    </div>

</div>

</body>

</html>
