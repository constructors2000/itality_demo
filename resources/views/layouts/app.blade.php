<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Amoritalia') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet"/>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        .logo {
            background-image: url("{{ url('/img/logo.png') }}");
            background-repeat: no-repeat;
            background-size: contain;
            height: 30px;
            width: 112px;
            position: absolute;
            /*margin-left: -55px;*/
        }

        @media (min-width: 768px) {
            .logo {
                position: inherit;
            }
        }
    </style>
    @yield('head')
    @stack('head')
</head>
<body>

<!-- Modal -->
<div class="modal fade" id="modal_1" tabindex="-1" role="dialog" aria-labelledby="showModal1Title"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content rounded-0">
            <div class="modal-body">
                @stack('modal_1')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary rounded-0" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="wrapper">

    @guest

    @else
        <nav id="sidebar" class="bg-dark text-white">
            <!-- Sidebar Header -->
            <div class="sidebar-header navbar navbar-expand-md bg-dark text-white"><h3>Amoritalia</h3></div>

            <div class="list-group list-group-flush">
                <a href="{{route('product.paginate')}}"
                   class="list-group-item list-group-item-action {{ (Route::current()->getName() == 'product.paginate') ? 'active' : '' }}">Products</a>
                @if(Auth::user()->hasAnyRole(['admin', 'vendor']))
                    <a href="{{route('vendor.paginate')}}"
                       class="list-group-item list-group-item-action {{ (Route::current()->getName() == 'vendor.paginate') ? 'active' : '' }}">Vendors</a>
                @endif
                @if(Auth::user()->hasAnyRole(['admin', 'supplier']))
                    {{--<a href="#" class="list-group-item list-group-item-action">Suppliers</a>--}}
                @endif
                @if(Auth::user()->hasRole('admin'))
                    <a href="{{route('admin.dashboard')}}"
                       class="list-group-item list-group-item-action {{ (Route::current()->getName() == 'admin.dashboard') ? 'active' : '' }}">Admin</a>
                @endif
            </div>

            <!-- Sidebar Links -->
            @stack('sidebar')
        </nav>

    @endguest

    <div id="content">

        <div id="app">
            <nav class="navbar navbar-expand-md navbar-dark bg-dark">

                <div class="w-100 d-flex justify-content-between">
                    @guest

                    @else
                        <button type="button" id="sidebarCollapse" class="burger sidebar-toggle">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                    @endguest
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <div class="logo"></div>
                    </a>
                    <button class="burger navbar-toggler rounded-0" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link {{ (Route::current()->getName() == 'register') ? 'active' : '' }}"
                                       href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>

            </nav>

            <main class=" @guest m-0 @else py-4  @endguest">
                @stack('header-extension')
                @yield('content')
                @stack('footer-extension')
            </main>
        </div>

    </div>
</div>
<!-- Scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"
        integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
        crossorigin="anonymous"></script>

<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"
        integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ"
        crossorigin="anonymous"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"
        integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY"
        crossorigin="anonymous"></script>

<script src="{{ asset('js/app.js') }}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"
        integrity="sha256-kQ6DQtOnXtjGYnAEMZQjpsioC75ND0K9I8MyjtdLCyk="
        crossorigin="anonymous"></script>
<script>

    $(document).ready(function () {
        appInit();
    });

    function appInit() {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
            $(this).toggleClass('active');
        });

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "6000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };


        $('.js-select2').select2({
            width: "100%"
        });
    };
</script>

@if(!empty($url))
    <script>
        window.history.pushState("", "", '{!! $url !!}');
    </script>
@endif

@yield('scripts')
@stack('scripts')
</body>
</html>
